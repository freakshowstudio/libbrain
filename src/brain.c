
#include "autoconf.h"

#include <stdlib.h>
#include <stdarg.h>

#include "../include/brain.h"


static struct brain_feedforward_layer*
create_layer(unsigned int num_nodes)
{
    struct brain_feedforward_layer* layer =
        malloc(sizeof(struct brain_feedforward_layer));
    if (layer == NULL)
    {
        return NULL;
    }
    layer->num_nodes = num_nodes;
    layer->nodes =
        malloc(sizeof(struct brain_neural_node) * num_nodes);
    if (layer->nodes == NULL)
    {
        free(layer);
        return NULL;
    }
    return layer;
}

struct brain_feedforward_network*
brain_create_feedforward_network(
    unsigned int num_inputs,
    unsigned int num_layers,
    ...)
{
    struct brain_feedforward_network* network =
        malloc(sizeof(struct brain_feedforward_network));
    if (network == NULL)
    {
        return NULL;
    }
    network->num_layers = num_layers;
    network->inputs =
        malloc(sizeof(float) * num_inputs);
    if (network->inputs == NULL)
    {
        free(network);
        return NULL;
    }
    network->layers =
        malloc(sizeof(struct brain_feedforward_layer) * num_layers);
    if (network->layers == NULL)
    {
        free(network->inputs);
        free(network);
        return NULL;
    }
    va_list valist;
    va_start(valist, num_layers);
    for (unsigned int i = 0; i < num_layers; ++i)
    {
        unsigned int n = va_arg(valist, unsigned int);
        struct brain_feedforward_layer* layer =
            create_layer(n);
        network->layers[i] = layer;
    }
    va_end(valist);

    return network;
}
