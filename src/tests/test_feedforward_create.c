
#include <stdlib.h>

#include "../../include/brain.h"

int main(int __unused argc, char** __unused argv)
{
    struct brain_feedforward_network* network =
        brain_create_feedforward_network(3, 3, 1, 2, 3);

    if (network == NULL)
    {
        return EXIT_FAILURE;
    }
    if (network->num_layers != 3)
    {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
