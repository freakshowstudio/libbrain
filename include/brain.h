#ifndef BRAIN_H
#define BRAIN_H


struct brain_neural_node
{
    float* inputs;
    float* weights;
    float output;
};

struct brain_feedforward_layer
{
    unsigned int num_nodes;
    struct brain_neural_node* nodes;
};

struct brain_feedforward_network
{
    unsigned int num_layers;
    float* inputs;
    struct brain_feedforward_layer** layers;
    float output;
};


struct brain_feedforward_network*
brain_create_feedforward_network(
    unsigned int num_inputs,
    unsigned int num_layers,
    ...);

#endif // BRAIN_H
